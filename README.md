# Docker workshop

## Highlights

- This **is a workshop**. Please come with a laptop that has the necessary installed software.
- Don't let the length of this document intimidate you :)
- Please follow **all of the installation instructions** in this document before coming to the workshop.
  This is only a 2-day workshop, and we have a lot of material to cover.
  Debugging docker/git/compose installation takes time away from all attendees.

## Initial setup

- Please create a login on [Gitlab](https://gitlab.com/)
- Please create a login on [Docker Hub](https://hub.docker.com/)

## Installation

You will need **3** things installed

- Docker
- Docker Compose
- [Git](https://git-scm.com/downloads)

Optionally, a good text editor.
I highly recommend [VS Code](https://code.visualstudio.com/) with the [Docker Extension](https://marketplace.visualstudio.com/items?itemName=PeterJausovec.vscode-docker).

### Docker
If you did install it, you can test your installation using (the version number may not line up exactly but you should see something like this):

```bash
> docker version;
Client:
 Version:           18.06.0-ce
 API version:       1.38
 Go version:        go1.10.3
 Git commit:        0ffa825
 Built:             Wed Jul 18 19:05:26 2018
 OS/Arch:           darwin/amd64
 Experimental:      true

Server:
 Engine:
  Version:          18.06.0-ce
  API version:      1.38 (minimum version 1.12)
  Go version:       go1.10.3
  Git commit:       0ffa825
  Built:            Wed Jul 18 19:13:46 2018
  OS/Arch:          linux/amd64
  Experimental:     true
```

### Git

If you already have git, you can test your installation using (the version number may not line up exactly but you should see something like this):

```bash
> git --version
git version 2.18.0
```

### Docker Compose

If you already have docker-compose, you can test your installation using (the version number may not line up exactly but you should see something like this):

```bash
> docker-compose --version
docker-compose version 1.22.0, build f46880f
```

## If you are still to install

### Docker installation

- For modern OS'es you can find download instructions [here](https://store.docker.com/search?offering=community&type=edition)
- If your OS version is **not** supported by the native clients then you will need to install `docker toolbox` - https://docs.docker.com/toolbox/overview/

There are two items of note

- Please treat this installation like any other. Different operating systems and different set ups (especially in company-issued laptops) can make this installation tricky. Google, and perhaps your desktop support team (if using a company-issued laptop) are your friends. Debugging this can be tricky, and I urge you to ensure that you get everything installed.
- Docker requires a slew of permissions to run. So please make sure you grant Docker **all** the permissions it needs

#### Docker for Windows Users

**This only applies to those who had to install `docker-engine`**.

`docker-engine` upon installation asks to install `git`.  You
**should** allow it to install Git if you don't already have Git
running on your machine.  This is due to a dependency that
`docker-toolbox` has on `bash.exe`.

`docker-toolbox ` will install a shortcut on your Desktop.  If you
already have Git installed, but still run into errors upon waking up
`docker-toolbox ` then you might need to follow up on
this [thread](https://github.com/docker/toolbox/issues/335).

Upon waking up Docker might ask for `Host needs VT-x/AMD-v enabled for
docker-machine to work on virtualbox`.  To fix this you will need to
tweak your BIOS settings.  You can find
instructions
[here](https://docs.fedoraproject.org/en-US/Fedora/13/html/Virtualization_Guide/sect-Virtualization-Troubleshooting-Enabling_Intel_VT_and_AMD_V_virtualization_hardware_extensions_in_BIOS.html).

As Docker wakes up it prompts for a bunch of permissions (to enable
the network and such).  Please ensure that you grant Docker all the
permissions it needs.

#### Docker for Linux Users

You might need to follow the
instructions
[here](https://docs.docker.com/engine/installation/linux/linux-postinstall/),
in particular the section under "Manage Docker as a non-root user".

### Optionally

If you installed the Docker (Native) Client, then you can optionally
click on the Docker icon in your toolbar and install `Kitematic`.
Simply click on it, and follow the instructions.

![kitematic](docs/images/kitematic.png )

## Test your installation

Once again, at the command prompt

```bash
docker version;
git --version;
docker-compose --version;
```

This will tell you if all went well.

## Warm up your engines!

**You want to do this BEFORE you show up for the workshop!!**
Running this over a hotel wifi connection might not go well.
Using the command (bash) prompt:

```bash
docker pull alpine:3.8;
docker pull jenkins:2.60.3;
docker pull mariadb:10.0.36;
docker pull mariadb:10.2.11;
docker pull mongo:3.4.5;
docker pull nginx:1.14-alpine;
docker pull openjdk:8u131-jdk;
docker pull openjdk:8u131-jre;
docker pull portainer/portainer:latest;
docker pull tomcat:9;
docker pull ubuntu:18.10;
```

## The final test

Once again, at the command prompt:

```bash
> docker run -d --name myjenkins -p 8080:8080 jenkins:2.60.3;
> docker logs -f myjenkins;
```

You should see the Jenkins logs flowing by eventually settling with the following:

```
Sep 18, 2018 11:38:19 PM hudson.model.UpdateSite updateData
INFO: Obtained the latest update center data file for UpdateSource default
Sep 18, 2018 11:38:19 PM hudson.WebAppMain$3 run
INFO: Jenkins is fully up and running
Sep 18, 2018 11:38:19 PM hudson.model.UpdateSite updateData
INFO: Obtained the latest update center data file for UpdateSource default
Sep 18, 2018 11:38:20 PM hudson.model.DownloadService$Downloadable load
INFO: Obtained the updated data file for hudson.tasks.Maven.MavenInstaller
Sep 18, 2018 11:38:20 PM hudson.model.DownloadService$Downloadable load
INFO: Obtained the updated data file for hudson.tools.JDKInstaller
Sep 18, 2018 11:38:20 PM hudson.model.AsyncPeriodicWork$1 run
INFO: Finished Download metadata. 7,798 ms
--> setting agent port for jnlp
--> setting agent port for jnlp... done
```

Visit http://localhost:8080 and see if you see the Jenkins login page.
If you do, you are all set to go!!

```bash
# ctrl-c to break out of the logs
docker container stop myjenkins;
docker container rm myjenkins;
```

Woot!!!
See you soon!!!
